package com.demo.rabbitmq;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class HeadersPublisher {

	public static void main(String[] args) throws IOException, TimeoutException {
		ConnectionFactory connectionFactory = new ConnectionFactory();
		Connection connection = connectionFactory.newConnection();
		Channel channel = connection.createChannel();

		Map<String, Object> headersMap = new HashMap<>();
		headersMap.put("item1", "mobile");
		headersMap.put("item2", "television");

		BasicProperties basicProperties = new BasicProperties();
		basicProperties = basicProperties.builder().headers(headersMap).build();

		String message = "Message for Mobile and TV";

		channel.basicPublish("Headers-Exchange", "", basicProperties, message.getBytes());

		channel.close();
		connection.close();

	}

}
